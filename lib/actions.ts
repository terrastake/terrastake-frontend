export const fetchProductDetails = async () => {
    try {
      const response = await fetch("https://mocki.io/v1/ece0be34-93bb-42f1-926a-6c4b417d89fa");
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  };
  