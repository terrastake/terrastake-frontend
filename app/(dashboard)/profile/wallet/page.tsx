import MyWallet from "@/components/invest/MyWallet";
import ProfilePageLayout from "@/components/profile/ProfilePageLayout";

function InvestmentsPage() {
  return (
    <ProfilePageLayout component={<MyWallet />} selectedMenuItem="wallet" />
  )
}

export default InvestmentsPage;