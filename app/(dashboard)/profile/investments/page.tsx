import MyInvestments from "@/components/invest/MyInvestments";
import ProfilePageLayout from "@/components/profile/ProfilePageLayout";

function InvestmentsPage() {
  return (
    <ProfilePageLayout component={<MyInvestments />} selectedMenuItem="investments" />
  )
}

export default InvestmentsPage;