import ProfilePageLayout from "@/components/profile/ProfilePageLayout";
import UserInformation from "@/components/profile/UserInformation";

function Profile() {
    return (
        <ProfilePageLayout component={<UserInformation />} selectedMenuItem="profile" />
    );
}

export default Profile;
