import DashboardLayout from '../../DashboardLayout';
import ProductPage from '@/components/products/ProductPage';

interface Params {
    id: string;
}

export default async function singleProduct({ params }: { params: Params }) {
    return (
        <DashboardLayout >
            <ProductPage productId={params.id} />
        </DashboardLayout>
    );
}