import ProductList from '@/components/products/ProductList';
import SearchBar from '../../components/search/SearchBar';
import DashboardLayout from './DashboardLayout';
import Banner from '@/components/banner/Banner';

function LandingPage() {
  return (
    <DashboardLayout>
      <div className="xs:bg-[#019] w-[90%] md:w-[85%] lg:w-[80%] xl:w-[75%] 2xl:w-[65%] mx-auto bg-[#F9F9FF] dark:bg-[#121B18] px-[1vw] py-[25px]">
      <SearchBar />
      <ProductList limit={4} title='🚀 Super VIP განცხადებები' className={"mt-[50px]"}/>
      <Banner />
      <ProductList limit={4} title='🆕 ახალი დამატებული განცხადებები' className={"mt-[50px]"}/>
      </div>
    </DashboardLayout>
  )
}

export default LandingPage;