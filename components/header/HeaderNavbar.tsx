import ThemeSwitcher from "./ThemeSwitcher";
import FeedOutlinedIcon from '@mui/icons-material/FeedOutlined';
import Link from "next/link";
import PermIdentityIcon from '@mui/icons-material/PermIdentity';


function HeaderNavbar() {
    return (
        <nav>
          <ul className="flex items-center gap-3 md:gap-10">
            <li>
              <ul className="flex items-center gap-3 md:gap-5">
                <ThemeSwitcher />
                  <Link href={"/profile"}>
                    <PermIdentityIcon fontSize="medium" />
                  </Link>
              </ul>
            </li>
          </ul>
        </nav>
    );
}

export default HeaderNavbar;