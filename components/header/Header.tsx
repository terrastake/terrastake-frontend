import Image from "next/image";
import Logo from "../../public/logo.png";
import Link from "next/link";
import HeaderNavbar from "./HeaderNavbar";

async function Header() {

  return (
    <header className="bg-[#F9F9FF] border-b-2 border-[#00000026] dark:border-[#ffffff26] dark:bg-[#121B18] w-full px-8 py-3 sticky top-0 z-10">
      <div className="w-[90%] md:w-[85%] lg:w-[80%] xl:w-[75%] 2xl:w-[65%] mx-auto flex items-center justify-between">
        <div className="flex items-center font-bold text-lg">
          <Link href="/">
            <Image src={Logo} alt="Company Logo" width={50} height={50} />
          </Link>
        </div>
        <HeaderNavbar />
      </div>
    </header>
  );
}

export default Header;
