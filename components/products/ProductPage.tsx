import { fetchProductDetails } from '@/lib/actions';
import ProductDetail from './ProductDetails';
import { Product } from './ProductList';

type ProductPageProps = {
  productId: string;
};

async function ProductPage({ productId }: ProductPageProps) {
  const allProducts: Product[] = await fetchProductDetails();
  const product: Product | undefined = allProducts.find(product => Number(product.id) === Number(productId));

  if (!product) {
    return <div>Product not found</div>;
  }

  return (
    <div className="flex flex-col md:flex-row w-[90%] md:w-[85%] lg:w-[80%] xl:w-[75%] 2xl:w-[65%] mx-auto gap-[30px] p-6">
      <ProductDetail product={product} />
    </div>
  );
}

export default ProductPage;
