import Link from "next/link";
interface ProductCardProps {
  id: number,
  authorName: string,
  location: string,
  description: string,
  images: string[];
  price: number,
  area: number,
}

function ProductCard({ id, description, price, location, area, images }: ProductCardProps) {
  const imageUrl = images[0];

  return (
    <div className="flex flex-col justify-between w-[220px] dark:bg-[#1D2024] shadow-lg rounded-lg cursor-pointer group relative">
      <Link href={`/product/${id}`}>
        <div className="w-full h-[168px] rounded-t-lg" style={{
          backgroundImage: `url(${imageUrl})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}>
        </div>
        <div className="py-[10px] px-[15px] flex flex-col justify-between gap-y-2">
          <div className="text-md h-7 overflow-hidden whitespace-nowrap overflow-ellipsis">{description}</div>
          <div className="font-bold mt-[14px] h-7 text-sm"> ფასი ${price}</div>
          <span className="text-gray-500 dark:text-[#ffffff26]">{location}</span>
          <hr />
          <div className="flex justify-between items-center">
            <span className="text-sm text-gray-500">🏡 {area} მ²</span>
          </div>
        </div>
      </Link>
    </div>
  );
}

export default ProductCard;
