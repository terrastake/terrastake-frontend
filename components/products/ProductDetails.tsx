import Link from "next/link";
import localFont from '@next/font/local';
import { Product } from "./ProductList";
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import PersonIcon from '@mui/icons-material/Person';
import CallIcon from '@mui/icons-material/Call';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover"
import { Button } from "../ui/button";

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' })

function ProductDetail({ product }: { product: Product }) {
  return (
    <div className="flex flex-col items-center p-6 w-full max-w-2xl mx-auto dark:bg-[#1D2024] shadow-lg rounded-lg">
      <Link href="/" className="self-start mb-6">
        <p className="text-[#26b571]">
          <ArrowBackIosNewIcon fontSize="small" />
          უკან დაბრუნება</p>
      </Link>
      <div
        className="w-full h-[300px] rounded-t-lg mb-4"
        style={{
          backgroundImage: `url(${product.images[0]})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      ></div>
      <div className={`text-xl text-[#191C20] dark:text-[#E2E2E9] font-semibold text-center ${mtavruli.className}`}>{product.description}</div>
      <div className="w-full p-4 flex justify-around items-center">
        <div className="flex flex-col justify-between mt-5">
          <span className={`text-xl text-[#191C20] dark:text-[#E2E2E9] font-regular ${mtavruli.className}`}>დეტალები</span>
          <span className="text-md text-gray-500 mb-2">ფასი: ${product.price}</span>
          <span className="text-md text-gray-500 mb-2">ადგილმდებარეობა: {product.location}</span>
          <span className="text-sm text-gray-500 mb-4">ფართი: {product.area}  მ²</span>
        </div>
        <Popover>
          <PopoverTrigger className="w-[40%] text-center bg-[#26b571] text-white rounded-lg p-[10px] cursor-pointer">ყიდვა</PopoverTrigger>
          <PopoverContent>
            <span className={`text-md text-[#191C20] dark:text-[#E2E2E9] font-regular self-center ${mtavruli.className}`}>TerraWallet</span>
            <br />
            <span className={`text-sm text-[#191C20] dark:text-[#E2E2E9] font-regular ${mtavruli.className}`}>გაითვალისწინეთ მინიმალური შენატანი უნდა იყოს $200</span>
            <br />
            <Link href={"/profile/investments"}>
            <Button variant={"outline"} className="bg-[#26b571] text-white" size="sm">
              საფულიდან გადახდა
            </Button>
            </Link>
            </PopoverContent>
        </Popover>
      </div>
      <div className="dark:bg-[#1D2024] w-[90%] md:w-[65%] lg:w-[80%] xl:w-[75%] 2xl:w-[90%] mx-auto shadow-xl p-[25px] flex flex-col rounded-lg dark:border-[#ffffff1f]">
        <span className="font-['mtavruli'] border-b-[#1e90ff] border-b-[2px] flex items-center gap-[8px] pb-[5px] w-[135px]">
          <PersonIcon className="text-[#1e90ff]" /> გამყიდველი
        </span>
        <div className="w-full flex mt-[25px] justify-between items-center">
          <div className="flex flex-1 items-center gap-1">
            <div className="flex flex-col items-center ml-[20px]">
              <span className={`text-md md:text-lg font-bold ${mtavruli.className}`}>
                {product.authorName}
              </span>
              <span className={`text-sm md:text-md text-gray-500 dark:text-[#ffffffbf] ${mtavruli.className}`}>+995 555 555 555</span>
            </div>
          </div>
          <div>
            <Link href="tel:+995 555 555 555" className="flex justify-center items-center gap-2 md:mr-[25px]">
              <CallIcon className="text-[#1e90ff]" fontSize='small' />
              <span className={`text-[#1e90ff] text-sm md:text-lg ${mtavruli.className}`}>დაკავშირება</span>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductDetail;