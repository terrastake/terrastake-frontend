import ProductCard from "./ProductCard";
import localFont from '@next/font/local';
import { fetchProductDetails } from '@/lib/actions';

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' });

export interface Product {
  id: number;
  authorName: string; 
  location: string;
  description: string;
  images: string[];
  price: number;
  area: number;
}

async function ProductList({ 
  icon, 
  title, 
  className = "", 
  addNewBtn = false,
  limit 
}: { 
  icon?: any; 
  title?: string; 
  className?: string; 
  addNewBtn?: boolean,
  limit?: number 
}) {
  let products: Product[] = [];

  try {
    products = await fetchProductDetails();
  } catch (error) {
    console.error('Error fetching product details:', error);
  }
  
  const gridStyle = {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit, minmax(200px, 1fr))",
    justifyContent: "center",
    columnGap: "30px",
  };

  const displayedProducts = limit ? products.slice(0, limit) : products;

  return (
    <section className={"w-full " + className}>
      {title && (
        <div className="flex items-center gap-15 uppercase">
          {icon ? icon : null}
          <span className={`text-xl text-[#191C20] dark:text-[#E2E2E9] font-bold ${mtavruli.className}`}>
            {title}
          </span>
        </div>
      )}
      <div className="w-full grid my-3 gap-y-4" style={gridStyle}>
        {displayedProducts.map(product => (
          <ProductCard
            key={product.id}
            id={product.id}
            authorName={product.authorName}
            description={product.description}
            price={product.price}
            location={product.location}
            area={product.area}
            images={product.images}
          />
        ))}
      </div>
    </section>
  );
}

export default ProductList;
