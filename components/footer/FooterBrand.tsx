import Image from 'next/image';
import Logo from '../../public/logo.png';
import localFont from '@next/font/local';

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' })

function FooterBrand() {

    return (
        <div className="w-[250px] flex flex-col gap-[15px] items-center">
            <div className="flex items-center justify-start gap-[10px]">
                <Image src={Logo} alt="Superman" width={100} height={100} className='w-[100px] h-[100px]' />
                <span className={`text-xl font-['Open Sans'] ${mtavruli.className}`}>TerraStake</span>
            </div>
            <div className="text-sm">
                @ TerraStake. 2024. <br />
                ყველა უფლება დაცულია.
            </div>
        </div>
    )
}

export default FooterBrand;