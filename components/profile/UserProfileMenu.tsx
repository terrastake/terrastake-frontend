import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import SavingsIcon from '@mui/icons-material/Savings';
import WalletIcon from '@mui/icons-material/Wallet';
import Link from 'next/link';

interface UserProfileMenuProps {
  selectedItem: string;
}

async function UserProfileMenu({ selectedItem }: UserProfileMenuProps) {

  const menuItems = [
    {
      id: "profile",
      icon: <PersonOutlineIcon />,
      title: 'ჩემი პროფილი',
      route: "/profile"
    },
    {
      id: "investments",
      icon: <SavingsIcon />,
      title: 'ჩემი ინვესტიციები',
      route: "/profile/investments"
    },
    {
      id: "wallet",
      icon: <WalletIcon />,
      title: 'ჩემი საფულე',
      route: "/profile/wallet"
    },
  ];

  return (
    <div className="w-full flex-4 flex flex-col border dark:border-[#ffffff1f] shadow-lg rounded-lg bg-[#FEFEFE] dark:bg-[#1D2024]">
      {menuItems.map((item) => (
        <Link href={item.route} key={item.id}>
          <div
            className={`flex items-center gap-x-3 px-4 py-3 border-b-[1px] dark:border-b-[#ffffff1f] transition ease-in-out cursor-pointer ${selectedItem === item.id ? "text-[#26b571] border-l-[2px] border-l-[#26b571] rounded-sm" : ""}`}
          >
            {item.icon}
            <span className="text-md font-['mtavruli']">{item.title}</span>
          </div>
        </Link>
      ))}
    </div>
  );
}

export default UserProfileMenu;
