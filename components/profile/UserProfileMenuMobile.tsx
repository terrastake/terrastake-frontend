'use client';

import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import WalletIcon from '@mui/icons-material/Wallet';
import SavingsIcon from '@mui/icons-material/Savings';

import { Swiper, SwiperSlide } from 'swiper/react';
import { A11y, Navigation } from 'swiper/modules';
import UserMobileProfileMenu from './UserMobileProfileMenu';

import 'swiper/css/navigation';
import 'swiper/css';

function SwiperUserProfileMenu({ selectedItem }: { selectedItem: string }) {
    const menuItems = [
        {
          id: "profile",
          icon: <PersonOutlineIcon />,
          title: 'ჩემი პროფილი',
          route: "/profile"
        },
        {
          id: "investments",
          icon: <SavingsIcon />,
          title: 'ჩემი ინვესტიციები',
          route: "/profile/investments"
        },
        {
          id: "wallet",
          icon: <WalletIcon />,
          title: 'ჩემი საფულე',
          route: "/profile/wallet"
        },
      ];

    return (
        <div className="flex justify-between items-center bg-white dark:bg-[#1D2024] border-[1px] dark:border-[#ffffff1f] shadow-lg rounded p-5 gap-[20px]">
            <Swiper
                modules={[Navigation, A11y]}
                spaceBetween={15}
                slidesPerView={3}
                navigation={{
                    nextEl: '.swiper-button-custom-next',
                    prevEl: '.swiper-button-custom-prev'
                }}
                className="w-full h-full flex items-center justify-center flex-row gap-[15px]"
            >
                {menuItems.map((menuItem, index) => (
                    <SwiperSlide key={index}>
                        <UserMobileProfileMenu
                            icon={menuItem.icon}
                            title={menuItem.title}
                            route={menuItem.route}
                            isSelected={selectedItem === menuItem.id}
                        />
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    )
}

export default SwiperUserProfileMenu;