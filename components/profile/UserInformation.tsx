'use client';

import EditIcon from '@mui/icons-material/Edit';
import CheckIcon from '@mui/icons-material/Check';
import { useState } from "react";

interface Message {
    ok: boolean;
    message: string;
}

function UserInformation() {
    const [message, setMessage] = useState<Message | undefined>(undefined);

    const [name, setName] = useState("დად");
    const [surname, setSurname] = useState("ინტროვერტ");
    const [address, setAddress] = useState("სილიკონ ველი");
    const [phone, setPhone] = useState("+995 555 555 555");

    const [nameEnabled, setNameEnabled] = useState(false);
    const [surnameEnabled, setSurnameEnabled] = useState(false);
    const [addressEnabled, setAddressEnabled] = useState(false);
    const [phoneEnabled, setPhoneEnabled] = useState(false);

    return (
        <div className="w-full flex flex-col border dark:border-[#ffffff1f] shadow-lg rounded-lg bg-[#FEFEFE] dark:bg-[#1D2024] p-8 mb-8">
            <h2 className="text-2xl font-['mtavruli'] font-semibold mb-[20px] text-center w-full">ჩემი პროფილი</h2>

            <form autoComplete='off' className="flex flex-col space-y-4 mt-[10px]" >
                {message ? <div className="w-full flex justify-center items-center">
                    <span className={`rounded px-8 py-1 text-white ${message.ok ? 'bg-[#388E3C]' : 'bg-[#EC6652]'}`}>{message.message}</span>
                </div> : <></>}
                <div className="h-[2px] bg-gradient-to-r from-white via-[#26b571] to-white"> </div>
                <label htmlFor="email" className="block text-md font-medium text-gray-700">ელ.ფოსტა</label>
                <input
                    type="email"
                    id="email"
                    name="email"
                    value="test@testinio"
                    required
                    readOnly
                    className="mt-1 block w-full px-3 py-2 bg-white dark:bg-[#1D2024] border border-gray-300 dark:border-[#ffffff1f] rounded-md shadow-sm focus:ring-[#26b571] focus:border-[#26b571] focus:outline-none"
                />
                <div className="flex items-center justify-between">
                    <div className="relative w-[40%]">
                        <label htmlFor="name" className="block text-md font-medium text-gray-700">სახელი</label>
                        <input
                            type="text"
                            id="name"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            {...nameEnabled ? { readOnly: false } : { readOnly: true }}
                            className="mt-2 block w-full px-3 py-2 bg-white dark:bg-[#1D2024] border border-gray-300 dark:border-[#ffffff1f] rounded-md shadow-sm focus:ring-[#26b571] focus:border-[#26b571] focus:outline-none"
                        />
                        {!nameEnabled ? <EditIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setNameEnabled(true)} /> : <></>}
                        {nameEnabled ? <CheckIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setNameEnabled(false)} /> : <></>}
                    </div>
                    <div className="relative w-[56%]">
                        <label htmlFor="surname" className="block text-md font-medium text-gray-700">გვარი</label>
                        <input
                            type="text"
                            id="surname"
                            value={surname}
                            onChange={(e) => setSurname(e.target.value)}
                            {...surnameEnabled ? { readOnly: false } : { readOnly: true }}
                            className="mt-2 block w-full px-3 py-2 bg-white dark:bg-[#1D2024] border border-gray-300 dark:border-[#ffffff1f] rounded-md shadow-sm focus:ring-[#26b571] focus:border-[#26b571] focus:outline-none"
                        />
                        {!surnameEnabled ? <EditIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setSurnameEnabled(true)} /> : <></>}
                        {surnameEnabled ? <CheckIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setSurnameEnabled(false)} /> : <></>}
                    </div>
                </div>
                <div className="relative">
                    <label htmlFor="address" className="block text-md font-medium text-gray-700">მისამართი</label>
                    <input
                        type="text"
                        id="address"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                        {...addressEnabled ? { readOnly: false } : { readOnly: true }}
                        className="mt-2 block w-full px-3 py-2 bg-white dark:bg-[#1D2024] border border-gray-300 dark:border-[#ffffff1f] rounded-md shadow-sm focus:ring-[#26b571] focus:border-[#26b571] focus:outline-none"
                    />
                    {!addressEnabled ? <EditIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setAddressEnabled(true)} /> : <></>}
                    {addressEnabled ? <CheckIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setAddressEnabled(false)} /> : <></>}
                </div>
                <div className="relative">
                    <label htmlFor="phone" className="block text-md font-medium text-gray-700">მობილურის ნომერი</label>
                    <input
                        type="text"
                        id="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        {...phoneEnabled ? { readOnly: false } : { readOnly: true }}
                        className="mt-2 block w-full px-3 py-2 bg-white dark:bg-[#1D2024] border border-gray-300 dark:border-[#ffffff1f] rounded-md shadow-sm focus:ring-[#26b571] focus:border-[#26b571] focus:outline-none"
                    />
                    {!phoneEnabled ? <EditIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setPhoneEnabled(true)} /> : <></>}
                    {phoneEnabled ? <CheckIcon className="absolute right-2 top-10 text-gray-500 cursor-pointer" onClick={() => setPhoneEnabled(false)} /> : <></>}
                </div>
                <button
                    type="submit"
                    className="w-[40%] self-center px-4 py-3 text-md font-medium text-white bg-[#26b571] rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2">
                    შენახვა
                </button>
            </form>
        </div>
    );
}

export default UserInformation;
