import Link from "next/link";

function UserMobileProfileMenu({ title, icon, isSelected } : { title: string, icon: JSX.Element, route: string, isSelected: boolean}) {
    return (
        <Link href="/" className={`flex flex-col justify-center items-center gap-[5px] border ${isSelected ? "border-[#26b571]" : ""} py-[1px] h-[70px] rounded-xl`}    >
            <span className={isSelected ? "text-[#288c5c]" : "text-[#26b571]"}>{icon}</span>
            <p className="text-xs text-center">{title}</p>
        </Link>
    )
}

export default UserMobileProfileMenu;