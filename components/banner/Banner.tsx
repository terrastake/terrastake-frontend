import Image from "next/image";
import BannerImg from "../../public/banner.jpg"

function Banner() {
  return (
    <section className="w-[100%] flex gap-2 mt-[40px]">
      <div className="flex-1 rounded-[15px] w-full">
        <Image src={BannerImg} width={1266} height={314} unoptimized alt="First Banner" className="w-[100%] rounded-[15px]" />
      </div>
    </section>
  );
}

export default Banner;