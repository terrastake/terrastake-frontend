import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import localFont from '@next/font/local';

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' })

function SearchBar() {

    return (
        <div>
            <div className="text-[#191C20] text-xl">
                <span className={`font-semibold dark:text-[#26b571] uppercase ${mtavruli.className}`}>
                    მოძებნე შენი მომავალი ინვესტიცია
                </span>
            </div>
            <form>
                <div className="w-full flex bg-[#F9F9FF] dark:bg-[#121B18] gap-3 lg:gap-8 h-[60px] mt-[15px]">
                    <input placeholder='ძებნა'
                        className="flex-grow border-[#26b571] border-2 outline-none text-[#191C20] text-sm text-color-[#000000b3] rounded-[12px] p-[6px] px-[12px] dark:bg-[#121B18] dark:text-[#E2E2E9]" />
                    <button type='submit' className={`bg-[#26b571] text-white flex items-center justify-center gap-[5px] cursor-pointer w-[150px] rounded-[12px] ${mtavruli.className}`}>
                        <SearchOutlinedIcon />
                       ძებნა
                    </button>
                </div>
            </form>
        </div>
    );
}

export default SearchBar;
