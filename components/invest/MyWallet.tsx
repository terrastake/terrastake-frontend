import { Card, CardHeader, CardContent } from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import Link from "next/link";
import localFont from "@next/font/local";

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' })

export default function Wallet() {
  return (
    <Card className="w-full flex flex-col border dark:border-[#ffffff1f] shadow-lg rounded-lg bg-[#FEFEFE] dark:bg-[#1D2024] p-4">
      <CardHeader className="bg-primary text-primary-foreground p-4 rounded-t-lg">
        <div className="flex items-center justify-between">
          <div className="space-y-1">
          <h2 className={`text-2xl ${mtavruli.className} uppercase font-semibold w-full`}>
                ჩემი საფულე
            </h2>
            <p className={`text-xl ${mtavruli.className} uppercase font-regular w-full`}>მართე ბალანსი და NFT</p>
          </div>
        </div>
      </CardHeader>
      <CardContent className="p-6 space-y-6">
        <div className="grid gap-4">
          <div className="flex items-center justify-between">
            <div className="space-y-1">
              <p className="text-sm text-muted-foreground">ბალანსი</p>
              <p className="text-3xl font-bold">$4,567.89</p>
            </div>
            <Button variant="outline" size="sm" className="bg-[#26b571] text-white">
              შეავსე
            </Button>
          </div>
          <div className="flex items-center justify-between">
            <div className="space-y-1">
              <p className="text-sm text-muted-foreground">NFT ბალანსი</p>
              <p className="text-3xl font-bold">12.34 ETH</p>
            </div>
            <Button variant="outline" size="sm" className="bg-[#26b571] text-white">
              სულ NFTs
            </Button>
          </div>
        </div>
        <Separator />
        <div className="grid gap-4">
          <div className="flex items-center justify-between">
            <p className="text-sm text-muted-foreground">უახლესი ტრანზაქციები</p>
            <Link href="#" className="text-sm text-primary underline" prefetch={false}>
              ყველა
            </Link>
          </div>
          <div className="grid gap-4">
            <div className="flex items-center justify-between">
              <div className="flex items-center gap-4">
                <div className="bg-muted rounded-full w-10 h-10 flex items-center justify-center">
                  <AttachMoneyIcon className="w-5 h-5 text-muted-foreground" />
                </div>
                <div className="space-y-1">
                  <p className="text-sm font-medium">დეპოზიტი</p>
                  <p className="text-sm text-muted-foreground">+$1,000.00</p>
                </div>
              </div>
              <p className="text-sm font-medium">2 დღის წინ</p>
            </div>
            <div className="flex items-center justify-between">
              <div className="flex items-center gap-4">
                <div className="bg-muted rounded-full w-10 h-10 flex items-center justify-center">
                  <CreditCardIcon className="w-5 h-5 text-muted-foreground" />
                </div>
                <div className="space-y-1">
                  <p className="text-sm font-medium">ყიდვა</p>
                  <p className="text-sm text-muted-foreground">-$250.00</p>
                </div>
              </div>
              <p className="text-sm font-medium">5 დღის წინ</p>
            </div>
          </div>
        </div>
      </CardContent>
    </Card>
  )
}