import ProductList from "../products/ProductList";
import localFont from "@next/font/local";

const mtavruli = localFont({ src: '../../public/fonts/mtavruli.ttf' })

async function MyInvestments() {
    return (
        <div className="w-full flex flex-col border dark:border-[#ffffff1f] shadow-lg rounded-lg bg-[#FEFEFE] dark:bg-[#1D2024] p-8">
            <h2 className={`text-2xl ${mtavruli.className} uppercase font-semibold mb-[20px] text-center w-full`}>
                ჩემი ინვესტიციები
            </h2>
            <ProductList addNewBtn={true} />
        </div>
    )
}

export default MyInvestments;